"""Top-level package for PyVenture GUI."""

__author__ = """Peter Bozsoky"""
__email__ = 'peter-bozsoky@posteo.de'
__version__ = '0.1.0'
