#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = ["Click>=7.0", "PySide2"]

setup_requirements = [
    "pytest-runner",
]

test_requirements = [
    "pytest>=3",
    "pytest-qt",
    "pytest-xvfb",
]

setup(
    author="Peter Bozsoky",
    author_email="peter-bozsoky@posteo.de",
    python_requires=">=3.5",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    description="-",
    entry_points={
        "console_scripts": ["pyventure-gui=pyventure_gui.cli:main",],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="pyventure_gui",
    name="pyventure_gui",
    packages=find_packages(include=["pyventure_gui", "pyventure_gui.*"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://github.com/-/pyventure_gui",
    version="0.1.0",
    zip_safe=False,
)
